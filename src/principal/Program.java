package principal;

import util.Calculator;

import java.util.Locale;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter radius: ");
        double radius = sc.nextDouble();

        double circumference = Calculator.circumference(radius);
        double volumeDaEsfera = Calculator.volume(radius);

        System.out.printf("Circumference: %.2f", circumference);
        System.out.printf("Volume: %.2f", volumeDaEsfera);
        System.out.printf("PI value: %.2f", Calculator.PI);
    }
}
