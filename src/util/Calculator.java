package util;

public class Calculator {

    public static final double PI = 3.1459;

    public static double circumference(double radius){
        return 2 * PI * radius;
    }

    public static double volume(double radius){
        return 4 / 3 * (PI * radius * radius * radius);
    }


}
